// Code generated by go-swagger; DO NOT EDIT.

package environment_management

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "gitlab.com/kathra/kathra/kathra-services/kathra-runtimemanager/kathra-runtimemanager-go/kathra-runtimemanager-interface/models"
)

// GetRuntimeEnvironmentsOKCode is the HTTP code returned for type GetRuntimeEnvironmentsOK
const GetRuntimeEnvironmentsOKCode int = 200

/*GetRuntimeEnvironmentsOK Runtimes environments

swagger:response getRuntimeEnvironmentsOK
*/
type GetRuntimeEnvironmentsOK struct {

	/*
	  In: Body
	*/
	Payload []*models.RuntimeEnvironment `json:"body,omitempty"`
}

// NewGetRuntimeEnvironmentsOK creates GetRuntimeEnvironmentsOK with default headers values
func NewGetRuntimeEnvironmentsOK() *GetRuntimeEnvironmentsOK {

	return &GetRuntimeEnvironmentsOK{}
}

// WithPayload adds the payload to the get runtime environments o k response
func (o *GetRuntimeEnvironmentsOK) WithPayload(payload []*models.RuntimeEnvironment) *GetRuntimeEnvironmentsOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get runtime environments o k response
func (o *GetRuntimeEnvironmentsOK) SetPayload(payload []*models.RuntimeEnvironment) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetRuntimeEnvironmentsOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if payload == nil {
		// return empty array
		payload = make([]*models.RuntimeEnvironment, 0, 50)
	}

	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}
