// Code generated by go-swagger; DO NOT EDIT.

package application_management

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "gitlab.com/kathra/kathra/kathra-services/kathra-runtimemanager/kathra-runtimemanager-go/kathra-runtimemanager-interface/models"
)

// GetApplicationsFromRuntimeEnvironmentOKCode is the HTTP code returned for type GetApplicationsFromRuntimeEnvironmentOK
const GetApplicationsFromRuntimeEnvironmentOKCode int = 200

/*GetApplicationsFromRuntimeEnvironmentOK Runtimes environments

swagger:response getApplicationsFromRuntimeEnvironmentOK
*/
type GetApplicationsFromRuntimeEnvironmentOK struct {

	/*
	  In: Body
	*/
	Payload []*models.RuntimeAppInstance `json:"body,omitempty"`
}

// NewGetApplicationsFromRuntimeEnvironmentOK creates GetApplicationsFromRuntimeEnvironmentOK with default headers values
func NewGetApplicationsFromRuntimeEnvironmentOK() *GetApplicationsFromRuntimeEnvironmentOK {

	return &GetApplicationsFromRuntimeEnvironmentOK{}
}

// WithPayload adds the payload to the get applications from runtime environment o k response
func (o *GetApplicationsFromRuntimeEnvironmentOK) WithPayload(payload []*models.RuntimeAppInstance) *GetApplicationsFromRuntimeEnvironmentOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get applications from runtime environment o k response
func (o *GetApplicationsFromRuntimeEnvironmentOK) SetPayload(payload []*models.RuntimeAppInstance) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetApplicationsFromRuntimeEnvironmentOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if payload == nil {
		// return empty array
		payload = make([]*models.RuntimeAppInstance, 0, 50)
	}

	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}
