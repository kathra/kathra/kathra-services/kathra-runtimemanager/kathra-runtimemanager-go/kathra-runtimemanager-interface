// Code generated by go-swagger; DO NOT EDIT.

package application_management

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"
)

// DeleteApplicationOKCode is the HTTP code returned for type DeleteApplicationOK
const DeleteApplicationOKCode int = 200

/*DeleteApplicationOK RuntimeAppInstance deleted

swagger:response deleteApplicationOK
*/
type DeleteApplicationOK struct {

	/*
	  In: Body
	*/
	Payload string `json:"body,omitempty"`
}

// NewDeleteApplicationOK creates DeleteApplicationOK with default headers values
func NewDeleteApplicationOK() *DeleteApplicationOK {

	return &DeleteApplicationOK{}
}

// WithPayload adds the payload to the delete application o k response
func (o *DeleteApplicationOK) WithPayload(payload string) *DeleteApplicationOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the delete application o k response
func (o *DeleteApplicationOK) SetPayload(payload string) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *DeleteApplicationOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}
