// Code generated by go-swagger; DO NOT EDIT.

package backup_management

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"io"
	"net/http"

	"github.com/go-openapi/runtime"
)

// DownloadBackupRuntimeEnvironmentsOKCode is the HTTP code returned for type DownloadBackupRuntimeEnvironmentsOK
const DownloadBackupRuntimeEnvironmentsOKCode int = 200

/*DownloadBackupRuntimeEnvironmentsOK Backup downloaded with success

swagger:response downloadBackupRuntimeEnvironmentsOK
*/
type DownloadBackupRuntimeEnvironmentsOK struct {

	/*
	  In: Body
	*/
	Payload io.ReadCloser `json:"body,omitempty"`
}

// NewDownloadBackupRuntimeEnvironmentsOK creates DownloadBackupRuntimeEnvironmentsOK with default headers values
func NewDownloadBackupRuntimeEnvironmentsOK() *DownloadBackupRuntimeEnvironmentsOK {

	return &DownloadBackupRuntimeEnvironmentsOK{}
}

// WithPayload adds the payload to the download backup runtime environments o k response
func (o *DownloadBackupRuntimeEnvironmentsOK) WithPayload(payload io.ReadCloser) *DownloadBackupRuntimeEnvironmentsOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the download backup runtime environments o k response
func (o *DownloadBackupRuntimeEnvironmentsOK) SetPayload(payload io.ReadCloser) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *DownloadBackupRuntimeEnvironmentsOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}
