// Code generated by go-swagger; DO NOT EDIT.

package volume_storage_management

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "gitlab.com/kathra/kathra/kathra-services/kathra-runtimemanager/kathra-runtimemanager-go/kathra-runtimemanager-interface/models"
)

// GetRuntimeVolumeOKCode is the HTTP code returned for type GetRuntimeVolumeOK
const GetRuntimeVolumeOKCode int = 200

/*GetRuntimeVolumeOK RuntimeVolume from RuntimeAppInstance

swagger:response getRuntimeVolumeOK
*/
type GetRuntimeVolumeOK struct {

	/*
	  In: Body
	*/
	Payload *models.RuntimeVolume `json:"body,omitempty"`
}

// NewGetRuntimeVolumeOK creates GetRuntimeVolumeOK with default headers values
func NewGetRuntimeVolumeOK() *GetRuntimeVolumeOK {

	return &GetRuntimeVolumeOK{}
}

// WithPayload adds the payload to the get runtime volume o k response
func (o *GetRuntimeVolumeOK) WithPayload(payload *models.RuntimeVolume) *GetRuntimeVolumeOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get runtime volume o k response
func (o *GetRuntimeVolumeOK) SetPayload(payload *models.RuntimeVolume) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetRuntimeVolumeOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
