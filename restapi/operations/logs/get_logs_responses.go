// Code generated by go-swagger; DO NOT EDIT.

package logs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"
)

// GetLogsOKCode is the HTTP code returned for type GetLogsOK
const GetLogsOKCode int = 200

/*GetLogsOK RuntimeAppService

swagger:response getLogsOK
*/
type GetLogsOK struct {

	/*
	  In: Body
	*/
	Payload string `json:"body,omitempty"`
}

// NewGetLogsOK creates GetLogsOK with default headers values
func NewGetLogsOK() *GetLogsOK {

	return &GetLogsOK{}
}

// WithPayload adds the payload to the get logs o k response
func (o *GetLogsOK) WithPayload(payload string) *GetLogsOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get logs o k response
func (o *GetLogsOK) SetPayload(payload string) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetLogsOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}
