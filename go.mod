module gitlab.com/kathra/kathra/kathra-services/kathra-runtimemanager/kathra-runtimemanager-go/kathra-runtimemanager-interface

go 1.13

require (
	github.com/ericchiang/k8s v1.2.0
	github.com/ghodss/yaml v1.0.0
	github.com/go-openapi/analysis v0.19.6 // indirect
	github.com/go-openapi/errors v0.19.2
	github.com/go-openapi/loads v0.19.4
	github.com/go-openapi/runtime v0.19.8
	github.com/go-openapi/spec v0.19.4
	github.com/go-openapi/strfmt v0.19.3
	github.com/go-openapi/swag v0.19.5
	github.com/go-openapi/validate v0.19.5
	github.com/go-swagger/go-swagger v0.21.0 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/jessevdk/go-flags v1.4.0
	github.com/kubernetes/client-go v11.0.0+incompatible // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.5.0 // indirect
	go.mongodb.org/mongo-driver v1.1.3 // indirect
	golang.org/x/net v0.0.0-20191119073136-fc4aabc6c914
	golang.org/x/sys v0.0.0-20191120155948-bd437916bb0e // indirect
	golang.org/x/tools v0.0.0-20191122232904-2a6ccf25d769 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
	k8s.io/client-go v11.0.0+incompatible // indirect
)
